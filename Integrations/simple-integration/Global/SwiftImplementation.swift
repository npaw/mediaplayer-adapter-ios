//
//  SwiftImplementation.swift
//  simple-integration
//
//  Created by Tiago Pereira on 30/09/2020.
//  Copyright © 2020 NicePeopleAtWork. All rights reserved.
//

import Foundation
import MediaPlayer
import YouboraLib
import YouboraMediaPlayerAdapter

class SwiftImplementation {
    
    static func setPlayer(player: MPMusicPlayerController) {
        let plugin = YBPlugin(options: YBOptions())
        
        let adapter = YBMediaPlayerAdapter(player: player)
        
        plugin.adapter = YBMediaPlayerAdapterSwiftTranformer.transform(from: adapter)
    }
}
