//
//  ObjcImplementation.m
//  simple-integration
//
//  Created by Tiago Pereira on 30/09/2020.
//  Copyright © 2020 NicePeopleAtWork. All rights reserved.
//

#import "ObjcImplementation.h"

@import YouboraLib;
@import YouboraMediaPlayerAdapter;

@implementation ObjcImplementation

+(void)setPlayer:(MPMusicPlayerController*)player {
    YBPlugin *plugin = [[YBPlugin alloc] initWithOptions:[YBOptions new]];
    
    plugin.adapter = [[YBMediaPlayerAdapter alloc] initWithPlayer:player];
}

@end
