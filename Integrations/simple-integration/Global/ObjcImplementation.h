//
//  ObjcImplementation.h
//  simple-integration
//
//  Created by Tiago Pereira on 30/09/2020.
//  Copyright © 2020 NicePeopleAtWork. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ObjcImplementation : NSObject

+(void)setPlayer:(MPMusicPlayerController*)player;

@end
