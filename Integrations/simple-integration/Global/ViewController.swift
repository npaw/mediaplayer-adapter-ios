//
//  ViewController.swift
//  simple-integration
//
//  Created by Tiago Pereira on 30/09/2020.
//  Copyright © 2020 NicePeopleAtWork. All rights reserved.
//

import UIKit
import MediaPlayer

class ViewController: UIViewController {
    
    let player = MPMusicPlayerController.applicationMusicPlayer
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        SwiftImplementation.setPlayer(player: player)
        ObjcImplementation.setPlayer(player)
    }
}

