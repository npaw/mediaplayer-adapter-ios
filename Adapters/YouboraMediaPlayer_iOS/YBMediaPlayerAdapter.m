//
//  YBMediaPlayerAdapter.m
//  YouboraMediaPlayerAdapter_iOS
//
//  Created by Tiago Pereira on 08/05/2020.
//  Copyright © 2020 npaw. All rights reserved.
//

#import "YBMediaPlayerAdapter.h"
#import "Utils.h"

@interface YBMediaPlayerAdapter()

@property MPMediaItem *currentItem;

@end

@implementation YBMediaPlayerAdapter

- (instancetype)initWithPlayer:(MPMusicPlayerController*)player {
    self = [super initWithPlayer:player];
    
    if (self) {
        self.supportPlaylists = true;
    }
    
    return self;
}
#pragma mark - Notifications Section
- (void)registerListeners {
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(stateDidChange:) name:MPMusicPlayerControllerPlaybackStateDidChangeNotification object:nil];
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(itemDidChange:) name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification object:nil];
}

- (void)unregisterListeners {
    [NSNotificationCenter.defaultCenter removeObserver:self];
}

-(void)stateDidChange:(NSNotification*)notification {
    MPMusicPlayerController *player = self.player;
    if (!player) {
        return;
    }
    
    switch (player.playbackState) {
        case MPMusicPlaybackStatePlaying:
            if (self.flags.seeking) {
                [self fireSeekEnd];
            }
            [self fireJoin];
            [self fireResume];
            break;
        case MPMusicPlaybackStatePaused:
            if (self.flags.seeking) {
                [self fireSeekEnd];
            }
            [self firePause];
            break;
        case MPMusicPlaybackStateInterrupted:
            [self firePause];
            break;
        case MPMusicPlaybackStateSeekingBackward:
            [self fireSeekBegin];
            break;
        case MPMusicPlaybackStateSeekingForward:
            [self fireSeekBegin];
            break;
        case MPMusicPlaybackStateStopped:
            [self fireStop];
            break;
    }
}

-(void)itemDidChange:(NSNotification*)notification {
    MPMusicPlayerController *player = self.player;
    if (!player || !player.nowPlayingItem) {
        return;
    }
    
    if (!self.currentItem) {
        self.currentItem = player.nowPlayingItem;
        [self fireStart];
        return;
    }
    
    if (self.supportPlaylists && player.nowPlayingItem != self.currentItem) {
        [self fireStop];
        self.currentItem = player.nowPlayingItem;
        [self fireStart];
        [self stateDidChange:nil];
    }
}

#pragma mark - Getters Section

- (NSString *)getPlayerVersion {
    return [Utils getPlayerName:false];
}

- (NSString *)getPlayerName {
    return [Utils getPlayerNameWithOs:false];
}

- (NSString *)getVersion {
    return [Utils getPlayerNameWithOsAndVersion:false];
}

- (NSValue *)getIsLive {
    NSValue *isLive = self.plugin.options.contentIsLive;
    
    return isLive != nil ? isLive : [NSNumber numberWithBool: false];
}

- (NSNumber *)getBitrate {
    return [super getBitrate];
}

- (NSString *)getResource {
    if (!self.currentItem) { return [super getResource];}
    
    return self.currentItem.assetURL.absoluteString;
}

- (NSString *)getTitle {
    if (!self.currentItem) { return [super getTitle];}
    return self.currentItem.title;
}

- (NSNumber *)getDuration {
     if (!self.currentItem) { return [super getDuration];}
    
    return [[NSNumber alloc] initWithDouble:self.currentItem.playbackDuration];
}

- (NSNumber *)getPlayrate {
    if (self.flags.paused || self.flags.stopped) {
        return [NSNumber numberWithInt:0];
    }
    
    return [NSNumber numberWithInt:1];
}

- (NSNumber *)getPlayhead {
    MPMusicPlayerController *player = self.player;
    if (!player) { return [super getPlayhead]; }
    
    return [[NSNumber alloc] initWithDouble:player.currentPlaybackTime];
}

@end
