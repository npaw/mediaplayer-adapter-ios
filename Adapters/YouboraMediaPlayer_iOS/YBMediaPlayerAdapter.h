//
//  YBMediaPlayerAdapter.h
//  YouboraMediaPlayerAdapter_iOS
//
//  Created by Tiago Pereira on 08/05/2020.
//  Copyright © 2020 npaw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
@import YouboraLib;


@interface YBMediaPlayerAdapter : YBPlayerAdapter<MPMusicPlayerController*>

@property bool supportPlaylists;

@end
