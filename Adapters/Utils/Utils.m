//
//  Utils.m
//  YouboraMediaPlayerAdapter_iOS
//
//  Created by Tiago Pereira on 10/05/2020.
//  Copyright © 2020 npaw. All rights reserved.
//

#import "Utils.h"

#define PLAYER_NAME "MediaPlayer"
#define ADAPTER_STRING_VERSION "6.5.1"

@implementation Utils

+(NSString*)getOSName {
#ifdef TARGET_OS_IOS
    return @"iOS";
#elif TARGET_OS_WATCHOS
    return @"tvOS";
#elif
    return @"unknown";
#endif
}

+(NSString*)getPlayerName:(Boolean)ads {
    if (ads) {
        return [NSString stringWithFormat:@"%@-Ads",@PLAYER_NAME];
    }
    return @PLAYER_NAME;
}
+(NSString*)getPlayerNameWithOs:(Boolean)ads {
    return [NSString stringWithFormat:@"%@-%@",[self getPlayerName:ads], [self getOSName]];
}
+(NSString*)getPlayerNameWithOsAndVersion:(Boolean)ads {
   return [NSString stringWithFormat:@"%@-%@",@ADAPTER_STRING_VERSION,[self getPlayerNameWithOs:ads]];
}

@end
