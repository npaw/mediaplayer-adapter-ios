//
//  Utils.h
//  YouboraMediaPlayerAdapter_iOS
//
//  Created by Tiago Pereira on 10/05/2020.
//  Copyright © 2020 npaw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+(NSString*)getPlayerName:(Boolean)ads;
+(NSString*)getPlayerNameWithOs:(Boolean)ads;
+(NSString*)getPlayerNameWithOsAndVersion:(Boolean)ads;

@end
