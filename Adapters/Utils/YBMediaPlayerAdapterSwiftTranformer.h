//
//  YBMediaPlayerAdapterSwiftTranformer.h
//  YouboraMediaPlayerAdapter_iOS
//
//  Created by Tiago Pereira on 10/05/2020.
//  Copyright © 2020 npaw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YBMediaPlayerAdapter.h"

@interface YBMediaPlayerAdapterSwiftTranformer : NSObject

+(YBPlayerAdapter<id>*)transformFromAdapter:(YBMediaPlayerAdapter*)adapter;

@end
