//
//  YouboraMediaPlayerAdapter.h
//  YouboraMediaPlayerAdapter_iOS
//
//  Created by Tiago Pereira on 08/05/2020.
//  Copyright © 2020 npaw. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for YouboraMediaPlayerAdapter.
FOUNDATION_EXPORT double YouboraMediaPlayerAdapterVersionNumber;

//! Project version string for YouboraMediaPlayerAdapter.
FOUNDATION_EXPORT const unsigned char YouboraMediaPlayerAdapterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YouboraMediaPlayerAdapter/PublicHeader.h>

#import <YouboraMediaPlayerAdapter/YBMediaPlayerAdapter.h>
#import <YouboraMediaPlayerAdapter/YBMediaPlayerAdapterSwiftTranformer.h>
