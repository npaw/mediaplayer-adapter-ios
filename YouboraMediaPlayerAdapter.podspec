Pod::Spec.new do |s|

  s.name         = "YouboraMediaPlayerAdapter"
  s.version = '6.5.1'

  # Metadata
  s.summary      = "Adapter to use YouboraLib on MPMusicPlayerController "

  s.description  = "<<-DESC
                    YouboraMediaPlayerAdapter is an adapter used 
                    for MPMusicPlayerController.
                    DESC"

  s.homepage     = "http://developer.nicepeopleatwork.com/"

  s.license      = { :type => "MIT", :file => "LICENSE.md" }

  s.author             = { "Nice People at Work" => "support@nicepeopleatwork.com" }

  # Platforms
  s.ios.deployment_target = "9.0"

  # Platforms
  s.swift_version = "4.0", "4.1", "4.2", "4.3", "5.0", "5.1"

  # Source Location
  s.source       = { :git => 'https://bitbucket.org/npaw/mediaplayer-adapter-ios.git', :tag => s.version}

  # Source files
  s.source_files = 'Adapters/**/*.{h,m,swift}'

  s.dependency 'YouboraLib', '~> 6.5.0'
end
