# YouboraMediaPlayerAdapter

A framework that will collect several video events from the MediaPlayer and send it to the back end

# Installation

#### CocoaPods

Insert into your Podfile

```bash
pod 'YouboraMediaPlayerAdapter'
```

and then run

```bash
pod install
```

#### Carthage

Insert into your Cartfile

```bash
git "https://bitbucket.org/npaw/mediaplayer-adapter-ios.git"
```

and then run

```bash
carthage update
```

when the update finishes go to **{YOUR_SCHEME} > Build Settings > Framework Search Paths** and add **\$(PROJECT_DIR)/Carthage/Build/{iOS, Mac, tvOS or the platform of your scheme}**

## How to use

## Start plugin and options

#### Swift

```swift

//Import
import YouboraLib

...

//Config Options and init plugin (do it just once for each play session)

var options: YBOptions {
        let options = YBOptions()
        options.contentResource = "http://example.com"
        options.accountCode = "accountCode"
        options.adResource = "http://example.com"
        options.contentIsLive = NSNumber(value: false)
        return options;
    }
    
var plugin = YBPlugin(options: self.options)
```

#### Obj-C

```objectivec

//Import
#import <YouboraLib/YouboraLib.h>

...

// Declare the properties
@property YBPlugin *plugin;

...

//Config Options and init plugin (do it just once for each play session)

YBOptions *options = [YBOptions new];
options.offline = false;
options.contentResource = resource.resourceLink;
options.accountCode = @"powerdev";
options.adResource = self.ad?.adLink;
options.contentIsLive = [[NSNumber alloc] initWithBool: resource.isLive];
        
self.plugin = [[YBPlugin alloc] initWithOptions:self.options];
```

For more information about the options you can check [here](http://developer.nicepeopleatwork.com/apidocs/ios6/Classes/YBOptions.html)


### YouboraMediaPlayerAdapter

#### Swift

```swift
import YouboraMediaPlayerAdapter

...

//Once you have your player and plugin initialized you can set the adapter
self.plugin?.adapter = YBMediaPlayerAdapterSwiftTranformer.transform(from: YBMediaPlayerAdapter(player: player))

...

// When the view gonna be destroyed you can force stop and clean the adapters in order to make sure you avoid retain cycles  
self.plugin.fireStop()
self.plugin.removeAdapter()
self.plugin.removeAdsAdapter()
```

#### Obj-C

```objectivec
@import YouboraMediaPlayerAdapter;

...

//Once you have your player and plugin initialized you can set the adapter
[ [self.plugin setAdapter: [[YBMediaPlayerAdapter alloc] initWithPlayer:player]];

...

// When the view gonna be destroyed you can force stop and clean the adapters in order to make sure you avoid retain cycles  
[self.plugin fireStop];
[self.plugin removeAdapter];
[self.plugin removeAdsAdapter];
```

## Run samples project

###### Via cocoapods

Navigate to the root folder and then execute: 


```bash
pod install
```

1. Now you have to go to your **target > General > Frameworks, Libraries and Embedded Content** and change the frameworks that you are using in cocoapods from **Embed & Sign** to **Do Not Embed**

###### Via carthage (Default)

Navigate to the root folder and then execute: 
```bash
carthage update && cd Samples && carthage update
```

---
**NOTES**

Recently we are having some problems trying to install streamroot by carthage. As workaround and case you face this problem as well you can [download](https://support.streamroot.io/hc/en-us/articles/360003880394-SDK-for-Apple-AVPlayer) directly the framework and add it to the folder Samples/Carthage/Build/iOS

Case you have problems with Swift headers not found, when you're trying to install via **carthage** please do the follow instructions: 

 1. Remove Carthage folder from the project
 2. Execute the follow command ```rm -rf ~/Library/Caches/org.carthage.CarthageKit```

---