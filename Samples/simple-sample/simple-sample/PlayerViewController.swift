//
//  PlayerViewController.swift
//  simple-sample
//
//  Created by Tiago Pereira on 30/09/2020.
//  Copyright © 2020 NicePeopleAtWork. All rights reserved.
//

import UIKit
import MediaPlayer
import StoreKit
import YouboraConfigUtils
import YouboraLib
import YouboraMediaPlayerAdapter

class PlayerViewController: UIViewController {
    @IBOutlet weak var songLabel: UILabel!
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var seekBackwardButton: UIButton!
    @IBOutlet weak var seekForwardButton: UIButton!
    
    var collection: MPMediaItemCollection?
    let player = MPMusicPlayerController.applicationMusicPlayer
    let songs = MPMediaQuery.songs()
    
    var ybPlugin: YBPlugin?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let options = YouboraConfigManager.getOptions()
        
        options.contentRendition = "customRendition"
        self.ybPlugin = YBPlugin(options: options)
        
        let adapter = YBMediaPlayerAdapter(player: player)
        
        self.ybPlugin?.adapter = YBMediaPlayerAdapterSwiftTranformer.transform(from: adapter)
        
        player.setQueue(with: songs)
        
        player.beginGeneratingPlaybackNotifications()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didChangeItemNotification(_:)),
            name: Notification.Name.MPMusicPlayerControllerNowPlayingItemDidChange,
            object: player
        )
    }
    
    @objc func didChangeItemNotification(_ notification: Notification) {
        guard let player = notification.object as? MPMusicPlayerController,
            let item = player.nowPlayingItem else {
                return
        }
        
        self.songLabel.text = item.title
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if self.isMovingFromParent {
            self.ybPlugin?.fireStop()
            self.ybPlugin?.removeAdapter()
            self.player.stop()
        }
        
    }
    
    @IBAction func didPress(_ sender: UIButton) {
        if (sender == self.playButton) {
            self.player.endSeeking()
            self.player.play()
            return
        }
        
        if (sender == self.pauseButton) {
            self.player.endSeeking()
            self.player.pause()
            return
        }
        
        if (sender == self.nextButton) {
            self.player.endSeeking()
            self.player.skipToNextItem()
            return
        }
        
        if (sender == self.backButton) {
            self.player.endSeeking()
            self.player.skipToPreviousItem()
            return
        }
        
        if (sender == self.seekForwardButton) {
           self.player.beginSeekingForward()
        }
        
        if (sender == self.seekBackwardButton) {
            self.player.beginSeekingBackward()
        }
    }
}

