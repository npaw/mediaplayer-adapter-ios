//
//  ViewController.swift
//  simple-sample
//
//  Created by Tiago Pereira on 30/09/2020.
//  Copyright © 2020 NicePeopleAtWork. All rights reserved.
//

import UIKit
import YouboraConfigUtils

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func pressToSettings(_ sender: UIButton) {
        self.navigationController?.show( YouboraConfigViewController.initFromXIB(), sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let playerViewController = segue.destination as? PlayerViewController {
            // setup things
            playerViewController.title = "Player"
        }
    }

}

